var regexs = [
    /[\.,-\/#!$%\^&\*;:{}=\-_`~()]\s*do you/gi,
    /[\.,-\/#!$%\^&\*;:{}=\-_`~()]\s*can you/gi,
    /[\.,-\/#!$%\^&\*;:{}=\-_`~()]\s*could you/gi,
    /[\.,-\/#!$%\^&\*;:{}=\-_`~()]\s*would you/gi,
    /[\.,-\/#!$%\^&\*;:{}=\-_`~()]\s*you could/gi,
    /[\.,-\/#!$%\^&\*;:{}=\-_`~()]\s*you would/gi,
    /\?/gi
]

function nativeSelector() {
    var elements = document.querySelectorAll("body, body *");
    var results = [];
    var child;
    for(var i = 0; i < elements.length; i++) {
        child = elements[i].childNodes[0];
        if(elements[i].hasChildNodes() && child.nodeType === 3) {
            results.push(child);
        }
    }
    return results;
}

function process(){
    var textnodes = nativeSelector(),
    _nv;

    for (var i = 0, len = textnodes.length; i<len; i++){
        currentNode = textnodes[i];
        _nv = currentNode.nodeValue;
        var p = currentNode.parentNode;

        if (p.classList.contains('nwRT')){
            continue;
        }

        var pn = p.parentNode;
        var skipIt = false;

        while(pn){
            if (pn.nodeType == 1 && pn.hasAttribute("g_editable")){
                skipIt = true;
                break;
            }
            pn = pn.parentNode;
        }

        if (skipIt){
            continue;
        }

        if ( _nv.length > 0 ) {
            for (var j = 0, jen = regexs.length; j<jen; j++){
                 if (regexs[j].test(_nv)){
                    var ele = document.createElement('span');
                    ele.style.backgroundColor = 'yellow';
                    ele.classList.add('nwRT');
                    ele.textContent = _nv;
                    p.insertBefore(ele,textnodes[i]);
                    currentNode.remove();
                    break;
                 }
            }
        }
    }
}

window.setInterval(function(){process()}, 100);
